package helpers

import (
	"bufio"
	"errors"
	"math"
	"math/rand"
	"os"
	"strings"

	"github.com/leekchan/accounting"
)

var (
	letterRunes = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")
)

func ReadLines(path string) ([]string, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	var lines []string
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}
	file.Close()
	return lines, scanner.Err()
}

func round(num float64) int {
	return int(num + math.Copysign(0.5, num))
}

func ToFixed(num float64, precision int) float64 {
	output := math.Pow(10, float64(precision))
	return float64(round(num*output)) / output
}

func RemoveDuplicateCateID(elements []int64) []int64 {
	encountered := map[int64]bool{}
	result := []int64{}

	for v := range elements {
		if encountered[elements[v]] == true {
			// Do not add duplicate.
		} else {
			encountered[elements[v]] = true
			result = append(result, elements[v])
		}
	}
	return result
}

func PrettyNumber(value int) string {
	ac := accounting.Accounting{Symbol: "", Precision: 0, Thousand: " "}
	return ac.FormatMoney(value)
}

func RemoveDuplicateString(elements []string) []string {
	encountered := map[string]bool{}
	result := []string{}

	for v := range elements {
		if encountered[elements[v]] == true {
			// Do not add duplicate.
		} else {
			encountered[elements[v]] = true
			result = append(result, elements[v])
		}
	}
	return result
}

func GetMiddleText(text, startFrom, endTo string) (string, error) {
	var err error
	startIndex := strings.Index(text, startFrom)
	if startIndex == -1 {
		err = errors.New("Not find needed text")
	} else {
		startIndex += len(startFrom)
		endIndex := strings.Index(text[startIndex:], endTo)
		endIndex += len(endTo) - 1
		text = text[startIndex:][:endIndex]
	}
	return text, err
}

func RandStringRunes(n int) string {
	b := make([]rune, n)
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	return string(b)
}
